package com.Java.examples;

import java.util.Scanner;

public class SwapIntWithoutTemp {
	
	public static void main(String[] args)
	{
		System.out.println("Enter two Integers: " );
		Scanner in =new Scanner(System.in);
		int a=in.nextInt();
		int b=in.nextInt();
		System.out.println("Values entered Before swap are: "+a+" ,"+b);
		in.close();
		swap(a,b);


	}

	public static void swap(int x, int y)
	{
		x=x+y;
		y=x-y;
		x=x-y;
		
		System.out.println("values after swap are: "+x+","+y);
	}

}
