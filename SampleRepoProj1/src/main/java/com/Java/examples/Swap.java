package com.Java.examples;

import java.util.Scanner;

public class Swap {
	
	public static void main(String[] args) {
		
		System.out.println("Enter two values: " );
		Scanner in =new Scanner(System.in);
		Object a=in.nextLine();
		Object b=in.nextLine();
		System.out.println("Values entered Befor swap are: "+a+" ,"+b);
		in.close();
		swap(a,b);
		
		
	}

	public static void swap(Object x, Object y)
	{
		Object temp= x;
		x=y;
		y=temp;
		System.out.println("values after swap are: "+x+","+y);
	}
}
