package com.Selenium.examples;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BrokenLinksExample {
	
	public static WebDriver driver;
	
	public static void main(String[] args)
	{
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		
		String url= "https://opensource-demo.orangehrmlive.com/";
		//driver.get("https://opensource-demo.orangehrmlive.com/");
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		
		List<String> allLinks=getAllLinks(driver,url);
		findBrokenLinks(driver, allLinks);
		
		driver.findElement(By.id("txtUsername")).sendKeys("Admin");
		driver.findElement(By.id("txtPassword")).sendKeys("admin123");
		driver.findElement(By.id("btnLogin")).click();
		
		url=driver.getCurrentUrl();
		allLinks=getAllLinks(driver,url);
		findBrokenLinks(driver,allLinks);
		
		
		driver.close();
		
	}
	public static List<String> getAllLinks(WebDriver driver,String url)
	{
		int count=0;
		int active=0;
		driver.get(url);
		List<WebElement> allLinkEle=driver.findElements(By.tagName("a"));
		List<String> allLinks=new ArrayList<String>();
		for(WebElement link:allLinkEle)
		{
			count++;
			String hrefValue=link.getAttribute("href");
			if(hrefValue!=null&&(!hrefValue.contains("mailto"))&&(!hrefValue.contains("javascript"))&&hrefValue!="#")
			{
				active++;
				allLinks.add(hrefValue);
				//System.out.println(hrefValue);
			}
		}
		System.out.println("Total no.of links is: "+count);
		System.out.println("Total no. of active links is : "+active);
		return allLinks;
	}
	public static void findBrokenLinks(WebDriver driver, List<String> allLinks)
	{
		for(String link:allLinks)
		{
			try {
				HttpURLConnection con=(HttpURLConnection) new URL(link).openConnection();
				con.connect();
				int responseCode=con.getResponseCode();
				String responseMessage=con.getResponseMessage();
				if(responseCode!=200 && responseCode>400)
				{
					System.out.println(link+" is a broken link");
				}
				else {
					System.out.println("status code of "+link+" is : "+responseCode);
					System.out.println("Status message is : "+responseMessage);
				}
				con.disconnect();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
	}
	
//	100-199 : informational status
//	200-299 : success status
//	300-399 : redirection status
//	400-499 : client errors
//	500-599 : server errors
//	
}
